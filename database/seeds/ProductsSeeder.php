<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\Product;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // sample category
      $electronic = Category::create(['title' => 'Electronic', 'parent_id' => 0]);
      $electronic->childs()->saveMany([
          new Category(['title' => 'Handphone']),
          new Category(['title' => 'Kindle']),
          new Category(['title' => 'Tablet']),
          new Category(['title' => 'Headset'])
      ]);


      // sample product
      $gadget = Category::where('title', 'Handphone')->first();
      $gadget1 = Product::create([
          'name' => 'Iphone 6',
          'model' => 'GSM',
          'photo'=>'iphone6.png',
          'weight' => rand(1,3) * 1000,
          'price' => 4500000]);
      $gadget2 = Product::create([
          'name' => 'Iphone 7',
          'model' => 'GSM',
          'photo'=>'iphone7.png',
          'weight' => rand(1,3) * 1000,
          'price' => 6500000]);
      $gadget3 = Product::create([
          'name' => 'Iphone 8',
          'model' => 'GSM',
          'photo'=>'iphone8.png',
          'weight' => rand(1,3) * 1000,
          'price' => 8500000]);
      $gadget4 = Product::create([
          'name' => 'Iphone 10',
          'model' => 'GSM',
          'photo'=>'iphone10.png',
          'weight' => rand(1,3) * 1000,
          'price' => 10000000]);
      $gadget->products()->saveMany([$gadget1, $gadget2, $gadget3, $gadget4]);

      // sample product
      $kindle = Category::where('title', 'Kindle')->first();
      $kindle1 = Product::create([
          'name' => 'Kindle Fire 7',
          'model' => 'HD 2 7"',
          'photo'=>'Kindle-7-in-Fire.png',
          'weight' => rand(1,3) * 1000,
          'price' => 4500000]);
      $kindle2 = Product::create([
          'name' => 'Kindle Fire 7',
          'model' => 'HDX 7"',
          'photo'=>'Kindle-7-in-Fire.png',
          'weight' => rand(1,3) * 1000,
          'price' => 6500000]);
      $kindle3 = Product::create([
          'name' => 'Kindle Paperwhite',
          'model' => 'Booklet',
          'photo'=>'kindle-paper.png',
          'weight' => rand(1,3) * 1000,
          'price' => 8500000]);
      $kindle4 = Product::create([
          'name' => 'Kindle Paperwhite',
          'model' => 'Booklet',
          'photo'=>'kindle-paper.png',
          'weight' => rand(1,3) * 1000,
          'price' => 10000000]);
      $kindle->products()->saveMany([$kindle1, $kindle2, $kindle3, $kindle4]);

      // copy image sample to public folder
      $from = database_path() . DIRECTORY_SEPARATOR . 'seeds' . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR;
      $to = public_path() . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR;
      File::copy($from . 'iphone6.png', $to . 'iphone6.png');
      File::copy($from . 'iphone7.png', $to . 'iphone7.png');
      File::copy($from . 'iphone8.png', $to . 'iphone8.png');
      File::copy($from . 'iphone10.png', $to . 'iphone10.png');
      File::copy($from . 'Kindle-7-in-Fire.png', $to . 'Kindle-7-in-Fire.png');
      File::copy($from . 'kindle-paper.png', $to . 'kindle-paper.png');
    }
}
