<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Address;
use App\Province;
use App\Regency;

class AddressesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
         Province::populate();
         Regency::populate();

         // sample address for customer
         $customer = User::where('email', 'customer@gmail.com')->first();
         $address1 = Address::create([
             'user_id' => $customer->id,
             'name' => 'Ahmad',
             'detail' => 'Mekarsari Rt 02/005 Cimanggis Depok',
             'province_id' => 9,
             'regency_id' => 115,
             'phone' => '87823451238',
         ]);

         $address2 = Address::create([
             'user_id' => $customer->id,
             'name' => 'Joko',
             'detail' => 'Mekarsari Rt 02/005 Cimanggis Depok',
             'province_id' => 9,
             'regency_id' => 115,
             'phone' => '87823451238',
         ]);
     }
}
