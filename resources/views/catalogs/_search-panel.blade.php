{!! Form::open(['url' => 'catalogs', 'method'=>'get']) !!}
        <div class="form-group {!! $errors->has('q') ? 'has-error' : '' !!}">
          {!! Form::text('q', $q, ['class'=>'form-control', 'placeholder'=>'Apa yang kamu cari?']) !!}
           {!! Form::submit('Cari', ['class'=>'btn btn-primary']) !!}
          {!! $errors->first('q', '<p class="help-block">:message</p>') !!}
        </div>
    {!! Form::close() !!}