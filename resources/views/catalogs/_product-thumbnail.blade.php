

<div class="thumbnail">
<div class="card">
  <img src="{{ $product->photo_path }}" class="img-responsive img-rounded" height="200px"  width="100%">
  <div class="card-block">
    <h4 class="card-title">{{ $product->name }}</h4>
    <p class="card-title">Model: {{ $product->model }}</p>
    <p class="card-title">Harga: <strong>Rp{{ number_format($product->price, 2) }}</strong></p>
    <p class="card-text">
       @foreach ($product->categories as $category)
        <span class="label label-primary">
        <i class="fa fa-btn fa-tags"></i>
        {{ $category->title }}</span>
      @endforeach
    </p>
    
     @include('layouts._customer-feature', ['partial_view'=>'catalogs._add-product-form', 'data' => compact('product')])
  </div>
</div>
</div>