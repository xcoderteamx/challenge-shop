<p>
  {!! Form::open(['url' => 'cart', 'method'=>'post', 'class'=>'form-inline']) !!}
    {!! Form::hidden('product_id', $product->id) !!}
	<div class="input-group">
      {!! Form::number('quantity', 1, ['class'=>'form-control', 'min' => 1, 'placeholder' => 'Jumlah order']) !!}
      <span class="input-group-addon">
      {!! Form::submit('Tambah ke Cart', []) !!}
      </span>
     </div>
  {!! Form::close() !!}
</p>

