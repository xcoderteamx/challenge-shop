<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = ['user_id', 'name', 'detail', 'province_id', 'regency_id', 'phone'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function province()
    {
        return $this->belongsToMany('App\Province');
    }
    
    public function regency()
    {
        return $this->belongsTo('App\Regency');
    }


}
