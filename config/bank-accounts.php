<?php
return [
    'bca' => [
        'title' => 'BCA',
        'bank' => 'BCA',
        'name' => 'Rian Firandika',
        'number' => '45454545'
    ],
    'bni' => [
        'title' => 'BNI',
        'bank' => 'BNI',
        'name' => 'Rian Firandika',
        'number' => '787687678'
    ],
    'mandiri' => [
        'title' => 'Mandiri',
        'bank' => 'Mandiri',
        'name' => 'Rian Firandika',
        'number' => '23223232'
    ],
    'atm-bersama' => [
        'title' => 'ATM Bersama',
        'bank' => 'Mandiri',
        'name' => 'Rian Firandika',
        'number' => '23223232'
    ],
];
