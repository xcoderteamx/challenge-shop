# Challenge Shop RDP

## Features
- Login & register
- Integrated API Raja Ongkir
- Get Province & regencies data with API Raja Ongkir
- Automaticly Fee by Distance
- Add to cart
- Checkout
- Shipping Address
- Payment Information
- Manage Products
- Manage Categories
- Manage Order statuses

## Installation
- Please check your internet
- Run in your terminal
```
$ composer install
```

- Generate Artisan key
```
$ php artisan key:generate
```

- Setup database connection in .env file
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=rdp
DB_USERNAME=root
DB_PASSWORD=myql
```

- Migrate tables with demo data
```
$ php artisan migrate --seed
```

- Run Server
```
$ php artisan serve
```

- Access it on
```
http://localhost:8000/
```

- Default password
```
password : secret

```

- Default admin
```
username : admin@gmail.com
password : secret
```

- Workflow Diagram
```
https://drive.google.com/file/d/1ROWaE76UCGVNImdRzn3hCfesUikY4tvG/view?usp=sharing
```